﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Teste.Models;

namespace Teste.Controllers
{
    public class TesteController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Logar([FromBody]Dictionary<string, string> dados)
        {
            string usuario = dados["usuario"];
            string senha = dados["senha"];

            Models.TesteModel t = new Models.TesteModel();
            bool logado = t.Autenticar(usuario, senha);
            string url = "";
            if (logado)
                url = "/Teste/IndexCadastrar";
            var retorno = new
            {
                logado = logado,
                url = url
            };
            return Json(retorno);
        }
        public IActionResult IndexCadastrar()
        {
            return View();
        }
    }
}
